var wines = require('../wines')
describe("Test search functionality", function(){
    it("should return a success code", function(done){
        wines.search("blossom hill", function(response){
            expect(response.code).toBe(200)
            done()
        })
    })
})